<%-- 
    Document   : index
    Created on : 16-dic-2018, 19.42.54
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
                  <c:choose>
                    <c:when test="${pagina == \"artista\"}">
                        
                            <a href="artistainfo.jsp">&nbsp;Artisti</a>
                       
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="artistainfo.jsp"> &nbsp;Artisti</a>
                        </li>
                    </c:otherwise>
                </c:choose>
    
               <c:choose>
                    <c:when test="${pagina == \"dipendente\"}">
                        
                            <a href="dipendenteinfo.jsp">&nbsp;Dipendenti</a>
                       
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="dipendenteinfo.jsp">&nbsp;Dipendenti</a>
                        </li>
                    </c:otherwise>
                </c:choose>
                        
                <c:choose>
                    <c:when test="${pagina == \"museo\"}">
                        
                            <a href="museoinfo.jsp">&nbsp;Musei</a>
                       
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="museoinfo.jsp">&nbsp;Musei</a>
                        </li>
                    </c:otherwise>
                </c:choose>
    
                   <c:choose>
                    <c:when test="${pagina == \"opera\"}">
                        
                            <a href="operainfo.jsp">&nbsp;Opere</a>
                       
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="operainfo.jsp">&nbsp;Opere</a>
                        </li>
                    </c:otherwise>
                </c:choose>
    
    </body>
</html>
