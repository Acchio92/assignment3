<%-- 
    Document   : artistainfo
    Created on : 16-dic-2018, 18.25.21
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Museo</title>
    </head>
    <body>
        <h1>Informazioni Museo</h1>
        <form action="./MuseoServlet" method="POST">
        <table>
            <tr>
                <td>Museo Id</td>
                <td><input type="text" name="id" value="${museo.id}"/></td>
            </tr>
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" value="${museo.nome}"/></td>
            </tr>
             <tr>
                <td>Indirizzo</td>
                <td><input type="text" name="indirizzo" value="${museo.indirizzo}"/></td>
            </tr>
            <tr>
                <td>Citta</td>
                <td><input type="text" name="citta" value="${museo.citta}"/></td>
            </tr>
             <tr>
                 <td colspan="2">
                     <input type="submit" name="action" value="Add"/>
                     <input type="submit" name="action" value="Edit"/>
                     <input type="submit" name="action" value="Delete"/>
                     <input type="submit" name="action" value="Search"/>
                 </td>
             
            </tr>
        </table>
        </form>
            <br>
            <table border="1">
                <th>Id</th>
                <th>Nome</th>
                <th>Indirizzo</th>
                <th>Citta</th>
                <c:forEach items="${allMuseo}" var="museo">
                    <tr>
                        <td>${museo.id}</td>
                        <td>${museo.nome}</td>
                        <td>${museo.indirizzo}</td>
                        <td>${museo.citta}</td>
                       
                    </tr>
                </c:forEach>
            </table>
            
            <li>
                    <a href="/MuseoCRUDApp/"> &nbsp;Home</a>
           </li>
            
    </body>
</html>
