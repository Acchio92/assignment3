<%-- 
    Document   : artistainfo
    Created on : 16-dic-2018, 18.25.21
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dipendente</title>
    </head>
    <body>
        <h1>Informazioni Dipendente</h1>
        <form action="./DipendenteServlet" method="POST">
        <table>
            <tr>
                <td>Dipendente Id</td>
                <td><input type="text" name="id" value="${dipendente.id}"/></td>
            </tr>
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" value="${dipendente.nome}"/></td>
            </tr>
             <tr>
                <td>Cognome</td>
                <td><input type="text" name="cognome" value="${dipendente.cognome}"/></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input type="text" name="email" value="${dipendente.email}"/></td>
            </tr>
             <tr>
                <td>Responsabile Sicurezza</td>
                <td><input type="text" name="addettosicurezza" value="${dipendente.addettosicurezza}"/></td>
            </tr>
             <tr>
                <td>Museo</td>
                <td><input type="text" name="museo" value="${dipendente.museo}"/></td>
            </tr>
             <tr>
                <td>Telefono</td>
                <td><input type="text" name="telefono" value="${dipendente.telefono}"/></td>
            </tr>
             <tr>
                 <td colspan="2">
                     <input type="submit" name="action" value="Add"/>
                     <input type="submit" name="action" value="Edit"/>
                     <input type="submit" name="action" value="Delete"/>
                     <input type="submit" name="action" value="Search"/>
                 </td>
             
            </tr>
        </table>
        </form>
            <br>
            <table border="1">
                <th>Id</th>
                <th>Nome</th>
                <th>Cognome</th>
                <th>Email</th>
                <th>Responsabile Sicurezza</th>
                <th>Museo</th>
                <th>Telefono</th>
                <c:forEach items="${allDipendente}" var="dipendente">
                    <tr>
                        <td>${dipendente.id}</td>
                        <td>${dipendente.nome}</td>
                        <td>${dipendente.cognome}</td>
                        <td>${dipendente.email}</td>
                        <td>${dipendente.addettosicurezza}</td>
                        <td>${dipendente.museo}</td>
                        <td>${dipendente.telefono}</td>
                       
                    </tr>
                </c:forEach>
            </table>
            
            <li>
                    <a href="/MuseoCRUDApp/"> &nbsp;Home</a>
                </li>
            
    </body>
</html>
