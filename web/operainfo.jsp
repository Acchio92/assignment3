<%-- 
    Document   : artistainfo
    Created on : 16-dic-2018, 18.25.21
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Opera</title>
    </head>
    <body>
        <h1>Informazioni Opera</h1>
        <form action="./OperaServlet" method="POST">
        <table>
            <tr>
                <td>Opera Id</td>
                <td><input type="text" name="id" value="${opera.id}"/></td>
            </tr>
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" value="${opera.nome}"/></td>
            </tr>
             <tr>
                <td>Valore</td>
                <td><input type="text" name="valore" value="${opera.valore}"/></td>
            </tr>
            <tr>
                <td>Artista</td>
                <td><input type="text" name="artista" value="${opera.artista}"/></td>
            </tr>
             <tr>
                <td>Museo</td>
                <td><input type="text" name="museo" value="${opera.museo}"/></td>
            </tr>
             <tr>
                <td>Anno</td>
                <td><input type="text" name="anno" value="${opera.anno}"/></td>
            </tr>
             <tr>
                 <td colspan="2">
                     <input type="submit" name="action" value="Add"/>
                     <input type="submit" name="action" value="Edit"/>
                     <input type="submit" name="action" value="Delete"/>
                     <input type="submit" name="action" value="Search"/>
                 </td>
             
            </tr>
        </table>
        </form>
            <br>
            <table border="1">
                <th>Id</th>
                <th>Nome</th>
                <th>Valore</th>
                <th>Artista</th>
                <th>Museo</th>
                <th>Anno</th>
                
                <c:forEach items="${allOpera}" var="opera">
                    <tr>
                        <td>${opera.id}</td>
                        <td>${opera.nome}</td>
                        <td>${opera.valore}</td>
                        <td>${opera.artista}</td>
                        <td>${opera.museo}</td>
                        <td>${opera.anno}</td>                                             
                    </tr>
                </c:forEach>
            </table>
            
            <li>
                    <a href="/MuseoCRUDApp/"> &nbsp;Home</a>
                </li>
            
    </body>
</html>
