<%-- 
    Document   : artistainfo
    Created on : 16-dic-2018, 18.25.21
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Artista</title>
    </head>
    <body>
        <h1>Informazioni Artista</h1>
        <form action="./ArtistaServlet" method="POST">
        <table>
            <tr>
                <td>Artista Id</td>
                <td><input type="text" name="id" value="${artista.id}"/></td>
            </tr>
            <tr>
                <td>Nome</td>
                <td><input type="text" name="nome" value="${artista.nome}"/></td>
            </tr>
             <tr>
                <td>Cognome</td>
                <td><input type="text" name="cognome" value="${artista.cognome}"/></td>
            </tr>
             <tr>
                 <td colspan="2">
                     <input type="submit" name="action" value="Add"/>
                     <input type="submit" name="action" value="Edit"/>
                     <input type="submit" name="action" value="Delete"/>
                     <input type="submit" name="action" value="Search"/>
                </td>
             
            </tr>
        </table>
        </form>
            <br>
            <table border="1">
                <th>Id</th>
                <th>Nome</th>
                <th>Cognome</th>
                <c:forEach items="${allArtista}" var="artista">
                    <tr>
                        <td>${artista.id}</td>
                        <td>${artista.nome}</td>
                        <td>${artista.cognome}</td>
                       
                    </tr>
                </c:forEach>
            </table>
            
            <li>
                    <a href="/MuseoCRUDApp/"> &nbsp;Home</a>
                </li>
            
    </body>
</html>
