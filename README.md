Processo e Sviluppo Software
Assignment 3 – 2st submission
Andrea Agostinacchio 837980

L’applicazione realizzata implementa le operazioni CRUD ovvero (create, read, update, delete) per un set di 4 entità: Museo, Opera, Artista, Dipendente.


Setup e avvio
Per poter testare e avviare il progetto, bisogna seguire le seguenti
indicazioni:
1) Installare NetBeans nel pc.
2) Configurare il Server (in questo caso si è scelto di utilizzare
GlassFish 5.0).
Andare su Services, tasto destro su Servers e aggiungere un
nuovo server scegliendo tra la lista Glassfish, scaricare
Glassfish 5.0 ed infine scegliere la location e concludere.
3) Configurare il Database incorporato su Netbeans.
Andare su JavaDB e creare un nuovo Database.
4) Dopo aver avviato il server GlassFish, tasto destro e andare
su View Domain Admin Console. Una volta aperta la pagina di
configurazione, recarsi nella sezione JDBC dove andremo a
creare una Connection Pool inserendovi le caratteristiche del
database precedentemente creato quali: Nome del db,
password del db, User, ServerName e la porta. Infine ci
basterà andare su Resource (sempre nella sezione JDBC) e
creare una nuova jdbc resource collegandola alla Pool creata
in precedenza.
