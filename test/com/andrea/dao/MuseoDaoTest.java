/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Museo;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrea
 */
public class MuseoDaoTest {
    
    public MuseoDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMuseo method, of class MuseoDao.
     */
    @Test
    public void testAddMuseo() throws Exception {
        System.out.println("Aggiungo museo");
        
         
        Museo museo = new Museo();
        
        museo.setId(7);
        museo.setNome("Chadeau");
        museo.setIndirizzo("cheateu nuveau");
        museo.setCitta("Telaviv");
         
                
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MuseoDaoLocal instance = (MuseoDaoLocal)container.getContext().lookup("java:global/classes/MuseoDao");
        instance.addMuseo(museo);
        container.close();
     
    }

    /**
     * Test of editMuseo method, of class MuseoDao.
     */
    @Test
    public void testEditMuseo() throws Exception {
        System.out.println("Modifico museo");
        
        Museo museo = new Museo();
        
        museo.setId(5);
        museo.setNome("New Museo");
        museo.setIndirizzo("Profstrett");
        museo.setCitta("Amsterdam");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MuseoDaoLocal instance = (MuseoDaoLocal)container.getContext().lookup("java:global/classes/MuseoDao");
        instance.editMuseo(museo);
        
        container.close();
       
    }

    /**
     * Test of deleteMuseo method, of class MuseoDao.
     */
    @Test
    public void testDeleteMuseo() throws Exception {
        System.out.println("Rimuovo museo");
        
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MuseoDaoLocal instance = (MuseoDaoLocal)container.getContext().lookup("java:global/classes/MuseoDao");
        instance.deleteMuseo(8);
        container.close();
        
    }

    /**
     * Test of getMuseo method, of class MuseoDao.
     */
    @Test
    public void testGetMuseo() throws Exception {
        System.out.println("Cerco museo");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        MuseoDaoLocal instance = (MuseoDaoLocal)container.getContext().lookup("java:global/classes/MuseoDao");
        
        instance.getMuseo(1);
        
        container.close();
    }
   
}
