/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Dipendente;
import com.andrea.model.Museo;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrea
 */
public class DipendenteDaoTest {
    
    public DipendenteDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addDipendente method, of class DipendenteDao.
     */
    @Test
    public void testAddDipendente() throws Exception {
        System.out.println("Aggiungo dipendente");
        
        Dipendente dipendente = new Dipendente();
        Dipendente addettosicurezza = new Dipendente();
        Museo museo = new Museo();
        
        museo.setId(1);
             
        addettosicurezza.setId(1);
                
        dipendente.setId(5);
        dipendente.setNome("nome1");
        dipendente.setCognome("cognome1");
        dipendente.setEmail("email1");
        dipendente.setAddettosicurezza(addettosicurezza);
        dipendente.setMuseo(museo);
        dipendente.setTelefono(000000);
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        DipendenteDaoLocal instance = (DipendenteDaoLocal)container.getContext().lookup("java:global/classes/DipendenteDao");
        instance.addDipendente(dipendente);
        container.close();
       
    }

    /**
     * Test of editDipendente method, of class DipendenteDao.
     */
    @Test
    public void testEditDipendente() throws Exception {
        System.out.println("Modifico dipendente");
                
        Dipendente dipendente = new Dipendente();
        Dipendente addettosicurezza2 = new Dipendente();
        Museo museo2 = new Museo();
        
        addettosicurezza2.setId(2);
        museo2.setId(2);
        
        dipendente.setId(3);
        dipendente.setNome("Nuovo nome");
        dipendente.setCognome("Nuovo cognome");
        dipendente.setEmail("nuova email1");
        dipendente.setAddettosicurezza(addettosicurezza2);
        dipendente.setMuseo(museo2);
        dipendente.setTelefono(23456);
        
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        DipendenteDaoLocal instance = (DipendenteDaoLocal)container.getContext().lookup("java:global/classes/DipendenteDao");
        instance.editDipendente(dipendente);
        container.close();

    }

    /**
     * Test of deleteDipendente method, of class DipendenteDao.
     */
    @Test
    public void testDeleteDipendente() throws Exception {
        System.out.println("Rimuovo dipendente");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        DipendenteDaoLocal instance = (DipendenteDaoLocal)container.getContext().lookup("java:global/classes/DipendenteDao");
        instance.deleteDipendente(6);
        container.close();

    }

    /**
     * Test of getDipendente method, of class DipendenteDao.
     */
    @Test
    public void testGetDipendente() throws Exception {
        System.out.println("Cerco dipendente");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        DipendenteDaoLocal instance = (DipendenteDaoLocal)container.getContext().lookup("java:global/classes/DipendenteDao");
        Dipendente expResult = null;
        
        Dipendente result = instance.getDipendente(1);
        
        container.close();
        
        
       }
   
}
