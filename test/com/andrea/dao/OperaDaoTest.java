/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Artista;
import com.andrea.model.Museo;
import com.andrea.model.Opera;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrea
 */
public class OperaDaoTest {
    
    public OperaDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addOpera method, of class OperaDao.
     */
    @Test
    public void testAddOpera() throws Exception {
        System.out.println("Aggiungo opera");
        
        Opera opera = new Opera();
        Artista artista = new Artista();
        Museo museo = new Museo();
        
        artista.setId(1);
        museo.setId(1);
        
        opera.setId(5);
        opera.setNome("nome1");
        opera.setValore(1000);
        opera.setArtista(artista);
        opera.setMuseo(museo);
        opera.setAnno(1992);
       
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        OperaDaoLocal instance = (OperaDaoLocal)container.getContext().lookup("java:global/classes/OperaDao");
        instance.addOpera(opera);
        container.close();
    }

    /**
     * Test of editOpera method, of class OperaDao.
     */
    @Test
    public void testEditOpera() throws Exception {
        System.out.println("Modifico opera");
        
        Opera opera = new Opera();
        Artista artista2 = new Artista();
        Museo museo2 = new Museo();
        
        artista2.setId(1);
        museo2.setId(1);
        
        opera.setId(3);
        opera.setNome("Nuova opera");
        opera.setValore(5000);
        opera.setArtista(artista2);
        opera.setMuseo(museo2);
        opera.setAnno(2000);
        
       
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        OperaDaoLocal instance = (OperaDaoLocal)container.getContext().lookup("java:global/classes/OperaDao");
        instance.editOpera(opera);
        container.close();
    }

    /**
     * Test of deleteOpera method, of class OperaDao.
     */
    @Test
    public void testDeleteOpera() throws Exception {
        System.out.println("Rimuovo opera");
 

        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        OperaDaoLocal instance = (OperaDaoLocal)container.getContext().lookup("java:global/classes/OperaDao");
        instance.deleteOpera(2);
        container.close();
    }

    /**
     * Test of getOpera method, of class OperaDao.
     */
    @Test
    public void testGetOpera() throws Exception {
        System.out.println("Cerco opera");
       
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        OperaDaoLocal instance = (OperaDaoLocal)container.getContext().lookup("java:global/classes/OperaDao");
        
        Opera result = instance.getOpera(1);
        
        container.close();
    }

      
}
