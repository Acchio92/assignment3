/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Artista;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrea
 */
public class ArtistaDaoTest {
    
    public ArtistaDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addArtista method, of class ArtistaDao.
     */
    @Test
    public void testAddArtista() throws Exception {
        
        System.out.println("Aggiungo artista");
        
        Artista artista = new Artista();
        
        artista.setId(7);
        artista.setNome("Prova");
        artista.setCognome("Prova");
          
              
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ArtistaDaoLocal instance = (ArtistaDaoLocal)container.getContext().lookup("java:global/classes/ArtistaDao");
        instance.addArtista(artista);
        container.close();
       
    }

     /**
     * Test of editArtista method, of class ArtistaDao.
     */
    @Test
    public void testEditArtista() throws Exception {
        
        System.out.println("Modifico artista");
        
        Artista artista = new Artista();
        
        artista.setId(5);
        artista.setNome("Federico");
        artista.setCognome("Biagi");
              
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ArtistaDaoLocal instance = (ArtistaDaoLocal)container.getContext().lookup("java:global/classes/ArtistaDao");
        instance.editArtista(artista);
        
        container.close();
        
    }
    
    
    
     /**
     * Test of RemoveArtista method, of class ArtistaDao.
     */
    @Test
    public void testRemoveArtista() throws Exception {
        
        System.out.println("Rimuovo artista");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ArtistaDaoLocal instance = (ArtistaDaoLocal)container.getContext().lookup("java:global/classes/ArtistaDao");
        
        instance.deleteArtista(8);
        
        container.close();
       
    }
    
    
    
    
     /**
     * Test of getArtista method, of class ArtistaDao.
     */
    @Test
    public void testGetArtista() throws Exception {
        System.out.println("Cerco artista");
        
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        ArtistaDaoLocal instance = (ArtistaDaoLocal)container.getContext().lookup("java:global/classes/ArtistaDao");
        
        instance.getArtista(1);
        
        container.close();
       
    }
    
   
    
}
