/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "OPERA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Opera.findAll", query = "SELECT o FROM Opera o"),
    @NamedQuery(name = "Opera.findById", query = "SELECT o FROM Opera o WHERE o.id = :id"),
    @NamedQuery(name = "Opera.findByNome", query = "SELECT o FROM Opera o WHERE o.nome = :nome"),
    @NamedQuery(name = "Opera.findByValore", query = "SELECT o FROM Opera o WHERE o.valore = :valore"),
    @NamedQuery(name = "Opera.lastId", query = "SELECT MAX (o.id) FROM Opera o"),
    @NamedQuery(name = "Opera.findByAnno", query = "SELECT o FROM Opera o WHERE o.anno = :anno")})
public class Opera implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NOME")
    private String nome;
    @Column(name = "VALORE")
    private Integer valore;
    @Column(name = "ANNO")
    private Integer anno;
    @JoinColumn(name = "ARTISTA", referencedColumnName = "ID")
    @ManyToOne
    private Artista artista;
    @JoinColumn(name = "MUSEO", referencedColumnName = "ID")
    @ManyToOne
    private Museo museo;

    public Opera() {
    }

    public Opera(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getValore() {
        return valore;
    }

    public void setValore(Integer valore) {
        this.valore = valore;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }

    public Museo getMuseo() {
        return museo;
    }

    public void setMuseo(Museo museo) {
        this.museo = museo;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Opera)) {
            return false;
        }
        Opera other = (Opera) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.andrea.model.Opera[ id=" + id + " ]";
    }
    
}
