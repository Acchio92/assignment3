/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "DIPENDENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dipendente.findAll", query = "SELECT d FROM Dipendente d"),
    @NamedQuery(name = "Dipendente.findById", query = "SELECT d FROM Dipendente d WHERE d.id = :id"),
    @NamedQuery(name = "Dipendente.findByNome", query = "SELECT d FROM Dipendente d WHERE d.nome = :nome"),
    @NamedQuery(name = "Dipendente.findByCognome", query = "SELECT d FROM Dipendente d WHERE d.cognome = :cognome"),
    @NamedQuery(name = "Dipendente.findByEmail", query = "SELECT d FROM Dipendente d WHERE d.email = :email"),
    @NamedQuery(name = "Dipendente.lastId", query = "SELECT MAX (d.id) FROM Dipendente d"),
    @NamedQuery(name = "Dipendente.findByTelefono", query = "SELECT d FROM Dipendente d WHERE d.telefono = :telefono")})
public class Dipendente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 50)
    @Column(name = "COGNOME")
    private String cognome;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "TELEFONO")
    private Integer telefono;
    @OneToMany(mappedBy = "addettosicurezza")
    private Collection<Dipendente> dipendenteCollection;
    @JoinColumn(name = "ADDETTOSICUREZZA", referencedColumnName = "ID")
    @ManyToOne
    private Dipendente addettosicurezza;
    @JoinColumn(name = "MUSEO", referencedColumnName = "ID")
    @ManyToOne
    private Museo museo;

    public Dipendente() {
    }

    public Dipendente(Integer id) {
        this.id = id;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    
    @XmlTransient
    public Collection<Dipendente> getDipendenteCollection() {
        return dipendenteCollection;
    }

    public void setDipendenteCollection(Collection<Dipendente> dipendenteCollection) {
        this.dipendenteCollection = dipendenteCollection;
    }

    public Dipendente getAddettosicurezza() {
        return addettosicurezza;
    }

    public void setAddettosicurezza(Dipendente addettosicurezza) {
        this.addettosicurezza = addettosicurezza;
    }

    public Museo getMuseo() {
        return museo;
    }

    public void setMuseo(Museo museo) {
        this.museo = museo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dipendente)) {
            return false;
        }
        Dipendente other = (Dipendente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
       // return "com.andrea.model.Dipendente[ id=" + id + " ]";
       return nome +" "+ cognome;
    }
    
}
