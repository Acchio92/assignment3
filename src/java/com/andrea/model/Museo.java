/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "MUSEO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Museo.findAll", query = "SELECT m FROM Museo m"),
    @NamedQuery(name = "Museo.findById", query = "SELECT m FROM Museo m WHERE m.id = :id"),
    @NamedQuery(name = "Museo.findByNome", query = "SELECT m FROM Museo m WHERE m.nome = :nome"),
    @NamedQuery(name = "Museo.findByIndirizzo", query = "SELECT m FROM Museo m WHERE m.indirizzo = :indirizzo"),
    @NamedQuery(name = "Museo.lastId", query = "SELECT MAX (m.id) FROM Museo m"),
    @NamedQuery(name = "Museo.findByCitta", query = "SELECT m FROM Museo m WHERE m.citta = :citta")})
public class Museo implements Serializable {
      

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 50)
    @Column(name = "INDIRIZZO")
    private String indirizzo;
    @Size(max = 50)
    @Column(name = "CITTA")
    private String citta;
    @OneToMany(mappedBy = "museo")
    private Collection<Dipendente> dipendenteCollection;
    @OneToMany(mappedBy = "museo")
    private Collection<Opera> operaCollection;

    public Museo() {
    }

    public Museo(Integer id) {
        this.id = id;
    }

     public Museo(Integer id, String nome, String indirizzo, String citta) {
        this.id = id;
        this.nome = nome;
        this.indirizzo = indirizzo;
        this.citta = citta;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

        
    @XmlTransient
    public Collection<Dipendente> getDipendenteCollection() {
        return dipendenteCollection;
    }

    public void setDipendenteCollection(Collection<Dipendente> dipendenteCollection) {
        this.dipendenteCollection = dipendenteCollection;
    }

    @XmlTransient
    public Collection<Opera> getOperaCollection() {
        return operaCollection;
    }

    public void setOperaCollection(Collection<Opera> operaCollection) {
        this.operaCollection = operaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Museo)) {
            return false;
        }
        Museo other = (Museo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
       // return "com.andrea.model.Museo[ id=" + id + " ]";
       return nome;
    }
    
}
