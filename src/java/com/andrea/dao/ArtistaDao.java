/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Artista;
import javax.ejb.Stateless;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 *
 * @author Andrea
 */
@Stateless
public class ArtistaDao implements ArtistaDaoLocal {
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addArtista(Artista artista) {
        em.persist(artista);
    }

    @Override
    public void editArtista(Artista artista) {
        em.merge(artista);
    }

    @Override
    public void deleteArtista(int id) {
        em.remove(getArtista(id));
    }

    @Override
    public Artista getArtista(int id) {
        return em.find(Artista.class, id);
    }

    @Override
    public List<Artista> getAllArtista() {
         return em.createNamedQuery("Artista.findAll").getResultList();
    }
 
    
    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Artista.lastId").getResultList().get(0)) + 1;
    }
    

}
