/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Dipendente;
import javax.ejb.Stateless;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 *
 * @author Andrea
 */
@Stateless
public class DipendenteDao implements DipendenteDaoLocal {
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addDipendente(Dipendente dipendente) {
        em.persist(dipendente);
    }

    @Override
    public void editDipendente(Dipendente dipendente) {
        em.merge(dipendente);
    }

    @Override
    public void deleteDipendente(int id) {
        em.remove(getDipendente(id));
    }

    @Override
    public Dipendente getDipendente(int id) {
        return em.find(Dipendente.class, id);
    }

    @Override
    public List<Dipendente> getAllDipendente() {
        return em.createNamedQuery("Dipendente.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Dipendente.lastId").getResultList().get(0)) + 1;
    }

    
}
