/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Museo;
import javax.ejb.Stateless;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 *
 * @author Andrea
 */
@Stateless
public class MuseoDao implements MuseoDaoLocal {
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addMuseo(Museo museo) {
        em.persist(museo);
    }

    @Override
    public void editMuseo(Museo museo) {
        em.merge(museo);
    }

    @Override
    public void deleteMuseo(int id) {
        em.remove(getMuseo(id));
    }

    @Override
    public Museo getMuseo(int id) {
        return em.find(Museo.class, id);
    }

    @Override
    public List<Museo> getAllMuseo() {
        return em.createNamedQuery("Museo.findAll").getResultList();
    }

    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Museo.lastId").getResultList().get(0)) + 1;
    }
    
}
