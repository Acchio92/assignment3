/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Museo;
import javax.ejb.Local;
import java.util.List;
/**
 *
 * @author Andrea
 */
@Local
public interface MuseoDaoLocal {

    void addMuseo(Museo museo);

    void editMuseo(Museo museo);

    void deleteMuseo(int id);

    Museo getMuseo(int id);

    List<Museo> getAllMuseo();
    
    int getId();
}
