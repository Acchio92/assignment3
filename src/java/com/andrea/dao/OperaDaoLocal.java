/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Opera;
import javax.ejb.Local;
import java.util.List;
/**
 *
 * @author Andrea
 */
@Local
public interface OperaDaoLocal {

    void addOpera(Opera opera);

    void editOpera(Opera opera);

    void deleteOpera(int id);

    Opera getOpera(int id);

    List<Opera> getAllOpera();
    
    int getId();
    
}
