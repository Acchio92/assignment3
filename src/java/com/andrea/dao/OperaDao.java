/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Opera;
import javax.ejb.Stateless;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 *
 * @author Andrea
 */
@Stateless
public class OperaDao implements OperaDaoLocal {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addOpera(Opera opera) {
        em.persist(opera);
    }

    @Override
    public void editOpera(Opera opera) {
        em.merge(opera);
    }

    @Override
    public void deleteOpera(int id) {
        em.remove(getOpera(id));
    }

    @Override
    public Opera getOpera(int id) {
        return em.find(Opera.class, id);
    }

    @Override
    public List<Opera> getAllOpera() {
        return em.createNamedQuery("Opera.findAll").getResultList();
    }
    @Override
    public int getId() {
        return ((int) em.createNamedQuery("Opera.lastId").getResultList().get(0)) + 1;
    }
    
}
