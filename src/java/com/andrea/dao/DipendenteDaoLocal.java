/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.dao;

import com.andrea.model.Dipendente;
import javax.ejb.Local;
import java.util.List;
/**
 *
 * @author Andrea
 */
@Local
public interface DipendenteDaoLocal {

    void addDipendente(Dipendente dipendente);

    void editDipendente(Dipendente dipendente);

    void deleteDipendente(int id);

    Dipendente getDipendente(int id);

    List<Dipendente> getAllDipendente();
    
    int getId();
}
