/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.controller;

import com.andrea.dao.ArtistaDaoLocal;
import com.andrea.model.Artista;
import java.io.IOException;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Andrea
 */
@WebServlet(name = "ArtistaServlet", urlPatterns = {"/ArtistaServlet"})
public class ArtistaServlet extends HttpServlet {

    @EJB
    private ArtistaDaoLocal artistaDao;
    
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String action = request.getParameter("action"); 
           
       int id = artistaDao.getId();
       String idStr = request.getParameter("id");
       if(idStr!=null && !idStr.equals("")){
           id=Integer.parseInt(idStr);
       }
       
       
       String nome = request.getParameter("nome");
       String cognome = request.getParameter("cognome");
       
       Artista artista = new Artista(id);
       artista.setNome(nome);
       artista.setCognome(cognome);
       
       if ("Add".equalsIgnoreCase(action)) {
            artistaDao.addArtista(artista);
        } else if ("Edit".equalsIgnoreCase(action)) {
            artistaDao.editArtista(artista);
        } else if ("Delete".equalsIgnoreCase(action)) {
            artistaDao.deleteArtista(id);
        } else if ("Search".equalsIgnoreCase(action)) {
                      
            artista = artistaDao.getArtista(id);
            
        }
       
        request.setAttribute("artista", artista);
        request.setAttribute("allArtista", artistaDao.getAllArtista());
        request.getRequestDispatcher("artistainfo.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
