/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.controller;

import com.andrea.dao.MuseoDaoLocal;
import com.andrea.model.Museo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrea
 */
@WebServlet(name = "MuseoServlet", urlPatterns = {"/MuseoServlet"})
public class MuseoServlet extends HttpServlet {

    @EJB
    private MuseoDaoLocal museoDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            
      String action = request.getParameter("action"); 
           
       int id = museoDao.getId();
       String idStr = request.getParameter("id");
       if(idStr!=null && !idStr.equals("")){
           id=Integer.parseInt(idStr);
       }
       String nome = request.getParameter("nome");
       String indirizzo = request.getParameter("indirizzo");
       String citta = request.getParameter("citta");
      
       
       Museo museo = new Museo(id);
       museo.setNome(nome);
       museo.setIndirizzo(indirizzo);
       museo.setCitta(citta);
       
       
       
       if ("Add".equalsIgnoreCase(action)) {
            museoDao.addMuseo(museo);
        } else if ("Edit".equalsIgnoreCase(action)) {
            museoDao.editMuseo(museo);
        } else if ("Delete".equalsIgnoreCase(action)) {
            museoDao.deleteMuseo(id);
        } else if ("Search".equalsIgnoreCase(action)) {
            
            museo = museoDao.getMuseo(id);
        }

        
        request.setAttribute("museo", museo);
        request.setAttribute("allMuseo", museoDao.getAllMuseo());
        request.getRequestDispatcher("museoinfo.jsp").forward(request, response);
            
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
