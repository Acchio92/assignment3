/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.controller;

import com.andrea.dao.ArtistaDaoLocal;
import com.andrea.dao.MuseoDaoLocal;
import com.andrea.dao.OperaDaoLocal;
import com.andrea.model.Opera;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrea
 */
@WebServlet(name = "OperaServlet", urlPatterns = {"/OperaServlet"})
public class OperaServlet extends HttpServlet {

    
    @EJB
    private OperaDaoLocal operaDao;
    @EJB
    private ArtistaDaoLocal artistaDao;
    @EJB
    private MuseoDaoLocal museoDao;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
 

        String action = request.getParameter("action"); 
           
       int id = operaDao.getId();
       String idStr = request.getParameter("id");
       if(idStr!=null && !idStr.equals("")){
           id=Integer.parseInt(idStr);
       }
       String nome = request.getParameter("nome");
       String cognome = request.getParameter("cognome");
              
       String valoreStr = request.getParameter("valore");
        int valore = 0;
        if (valoreStr != null && !valoreStr.equals("")) {
            valore = Integer.parseInt(valoreStr);
        } 
        
        String artista = request.getParameter("artista");
        int artistaid = 0;
        if (artista != null && !artista.equals("")) {
            artistaid = Integer.parseInt(artista);
        }
        
        String museo = request.getParameter("museo");
        int museoid = 0;
        if (museo != null && !museo.equals("")) {
            museoid = Integer.parseInt(museo);
        }
        
        String annoStr = request.getParameter("anno");
        int anno = 0;
        if (annoStr != null && !annoStr.equals("")) {
            anno = Integer.parseInt(annoStr);
        }
        
       Opera opera = new Opera(id);
       opera.setNome(nome);
       opera.setValore(valore);
       opera.setAnno(anno);
       opera.setArtista(artistaDao.getArtista(artistaid));
       opera.setMuseo(museoDao.getMuseo(museoid));

       
       if ("Add".equalsIgnoreCase(action)) {
            operaDao.addOpera(opera);
        } else if ("Edit".equalsIgnoreCase(action)) {
            operaDao.editOpera(opera);
        } else if ("Delete".equalsIgnoreCase(action)) {
            operaDao.deleteOpera(id);
        } else if ("Search".equalsIgnoreCase(action)) {
            
            
            opera = operaDao.getOpera(id);
        }

        
        request.setAttribute("opera", opera);
        request.setAttribute("allOpera", operaDao.getAllOpera());
        request.getRequestDispatcher("operainfo.jsp").forward(request, response);
        
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
