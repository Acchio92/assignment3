/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andrea.controller;

import com.andrea.dao.DipendenteDaoLocal;
import com.andrea.dao.MuseoDaoLocal;
import com.andrea.model.Dipendente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Andrea
 */
@WebServlet(name = "DipendenteServlet", urlPatterns = {"/DipendenteServlet"})
public class DipendenteServlet extends HttpServlet {

    @EJB
    private DipendenteDaoLocal dipendenteDao;
    @EJB
    private MuseoDaoLocal museoDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            
       String action = request.getParameter("action"); 
           
       int id = dipendenteDao.getId();
       String idStr = request.getParameter("id");
       if(idStr!=null && !idStr.equals("")){
           id=Integer.parseInt(idStr);
       }
       String nome = request.getParameter("nome");
       String cognome = request.getParameter("cognome");
       String email = request.getParameter("email");
       
       String telefonoStr = request.getParameter("telefono");
        int telefono = 0;
        if (telefonoStr != null && !telefonoStr.equals("")) {
            telefono = Integer.parseInt(telefonoStr);
        } 
        
        String addettosicurezza = request.getParameter("addettosicurezza");
        int addettosicurezzaid = 0;
        if (addettosicurezza != null && !addettosicurezza.equals("")) {
            addettosicurezzaid = Integer.parseInt(addettosicurezza);
        }
        
        String museo = request.getParameter("museo");
        int museoid = 0;
        if (museo != null && !museo.equals("")) {
            museoid = Integer.parseInt(museo);
        }
        
        
       Dipendente dipendente = new Dipendente(id);
       dipendente.setNome(nome);
       dipendente.setCognome(cognome);
       dipendente.setEmail(email);
       dipendente.setTelefono(telefono);
       dipendente.setAddettosicurezza(dipendenteDao.getDipendente(addettosicurezzaid));
       dipendente.setMuseo(museoDao.getMuseo(museoid));
       
       
       if ("Add".equalsIgnoreCase(action)) {
            dipendenteDao.addDipendente(dipendente);
        } else if ("Edit".equalsIgnoreCase(action)) {
            dipendenteDao.editDipendente(dipendente);
        } else if ("Delete".equalsIgnoreCase(action)) {
            dipendenteDao.deleteDipendente(id);
        } else if ("Search".equalsIgnoreCase(action)) {
            
            
            dipendente = dipendenteDao.getDipendente(id);
        }

        
        request.setAttribute("dipendente", dipendente);
        request.setAttribute("allDipendente", dipendenteDao.getAllDipendente());
        request.getRequestDispatcher("dipendenteinfo.jsp").forward(request, response);
            

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
